function Generate(
    data) {
        let data1 = data;
        let HTMLArray = [];
        const para1Array = data["text1"].split(" ");
        const para2Array = data["text2"].split(" ");
        const para3Array = data["text3"].split(" ");

        let para1LinkCount = 1;
        let para2LinkCount = 1;
        let para3LinkCount = 1;
        // check on paragraph 1
        for (let i = 0; i < para1Array.length; i++) {
            let linkPath = `linkSRC1-${para1LinkCount}`;
            let currentLink = `${data1['paraL1'][linkPath]}`;
            if (para1Array[i].includes("LINK*")) {
                para1Array[i] = para1Array[i].replace(
                    "LINK*",
                    `<a href="${currentLink}">`
                    );
            }
            if (para1Array[i].includes('*')) {
                para1Array[i] = para1Array[i].replace("*","</a>");
                para1LinkCount += 1;
            }
        }
        let para1 = para1Array.join(" ");
        // check paragraph 2
        for (let i = 0; i < para2Array.length; i++) {
            let linkPath = `linkSRC2-${para2LinkCount}`;
            let currentLink = `${data1['paraL2'][linkPath]}`;
            if (para2Array[i].includes("LINK*")) {
                para2Array[i] = para2Array[i].replace(
                    "LINK*",
                    `<a href="${currentLink}">`
                    );
            }
            if (para2Array[i].includes('*')) {
                para2Array[i] = para2Array[i].replace("*","</a>");
                para2LinkCount += 1;
            }
        }
        let para2 = para2Array.join(" ");
        // check paragraph 3
        for (let i = 0; i < para3Array.length; i++) {
            let linkPath = `linkSRC3-${para3LinkCount}`;
            let currentLink = `${data1['paraL3'][linkPath]}`;
            if (para3Array[i].includes("LINK*")) {
                para3Array[i] = para3Array[i].replace(
                    "LINK*",
                    `<a href="${currentLink}">`
                    );
            }
            if (para3Array[i].includes('*')) {
                para3Array[i] = para3Array[i].replace("*","</a>");
                para3LinkCount += 1;
            }
        }
        let para3 = para3Array.join(" ");

        // Combine parts into an array to easier clean display
        HTMLArray.push(`<h1><i>${data1.title}</i></h1>`);

        if (para1.length > 0){
        HTMLArray.push(`<h3>${data1.paraT1}</h3>`);
        HTMLArray.push(`<p>${para1}</p>`);
        }

        if (para2.length > 0){
        HTMLArray.push(`<h3>${data1.paraT2}</h3>`);
        HTMLArray.push(`<p>${para2}</p>`);
        }

        if (para3.length > 0){
        HTMLArray.push(`<h3>${data1.paraT3}</h3>`);
        HTMLArray.push(`<p>${para3}</p>`);
        }

        HTMLArray.push(`<p>${data1.conclusion}</p>`);

    return HTMLArray;
}


export default Generate;
