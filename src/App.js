import './App.css';
import Generate from './Generate';
import React, { useState, useEffect } from "react";

function App() {
  const [makeTitle, setTitleForm] = useState(true);
  const [firstForm, setForm] = useState(false);
  const [secondForm, setFormTwo] = useState(false);
  const [generatedHTML, setgenerate] = useState(false);
  const [link1Added, setLink1] = useState(false);
  const [link2Added, setLink2] = useState(false);
  const [link3Added, setLink3] = useState(false);
  const [link1Count, setlink1Count] = useState(0);
  const [link2Count, setlink2Count] = useState(0);
  const [link3Count, setlink3Count] = useState(0);
  const [paragraphCount, setCount] = useState(0);

  const [title, setTitle] = useState("");
  const [conclusion, setConclusion] = useState("");
  const [paraTitle1, setPara1] = useState("");
  const [para1Links, setPara1Links] = useState({});
  const [paraTitle2, setPara2] = useState("");
  const [para2Links, setPara2Links] = useState({});
  const [paraTitle3, setPara3] = useState("");
  const [para3Links, setPara3Links] = useState({});
  const [text1, setText1] = useState("");
  const [text2, setText2] = useState("");
  const [text3, setText3] = useState("");
  const [paragraphArray, setParaArray] = useState([]);

  const toTitleForm = async () => {
    setTitleForm(false);
    setForm(true);
  }


  const handleLink = async (e) => {
    e.preventDefault();
    if (e.target.value == 1) {
      var element = document.getElementById("paragraph1");
      let newText = element.value + ' LINK*Insert Name*';
      setText1(newText);
      element.value = newText;
      if (!link1Added) {
        setLink1(true);
      }
      let count = link1Count;
      setlink1Count(count + 1);
    } else if (e.target.value == 2) {
      var element = document.getElementById("paragraph2");
      let newText = element.value + ' LINK*Insert Name*';
      setText2(newText);
      element.value = newText;
      if (!link2Added) {
        setLink2(true);
      }
      let count = link2Count;
      setlink2Count(count + 1);
    } else if (e.target.value == 3) {
      var element = document.getElementById("paragraph3");
      let newText = element.value + ' LINK*Insert Name*';
      setText3(newText);
      element.value = newText;
      if (!link3Added) {
        setLink3(true);
      }
      let count = link3Count;
      setlink3Count(count + 1);
    }
  }

  const handleParaTile = async (e) => {
    if (e.target.id === "paragraph1Header") {
      setPara1(e.target.value);
    } else if (e.target.id === "paragraph2Header") {
      setPara2(e.target.value);
    } else if (e.target.id === "paragraph3Header") {
      setPara3(e.target.value);
    }
  }

  const handlelinkURL = async (e) => {
    if (e.target.id.includes("linkSRC1")) {
      let listLinks = para1Links;
      listLinks[e.target.id] = e.target.value;
      setPara1Links(listLinks);
    } else if (e.target.id.includes("linkSRC2")) {
      let listLinks = para2Links;
      listLinks[e.target.id] = e.target.value;
      setPara2Links(listLinks);
    } else if (e.target.id.includes("linkSRC3")) {
      let listLinks = para3Links;
      listLinks[e.target.id] = e.target.value;
      setPara3Links(listLinks);
    }
  }

  const handleText = async (e) => {
    if (e.target.id == "paragraph1"){
      setText1(e.target.value);
    } else if (e.target.id == "paragraph2"){
      setText2(e.target.value);
    } else if (e.target.id == "paragraph3"){
      setText3(e.target.value);
    }
  }

  const handleConclusion = async (e) => {
    setConclusion(e.target.value);
  }

  const generate = async (e) => {
    e.preventDefault();
    setFormTwo(false);
    setgenerate(true);
    const data = {
      title:title,
      conclusion:conclusion,
      paraT1:paraTitle1,
      paraT2:paraTitle2,
      paraT3:paraTitle3,
      paraL1:para1Links,
      paraL2:para2Links,
      paraL3:para3Links,
      text1:text1,
      text2:text2,
      text3:text3
    };
    setParaArray(Generate(data));
    setTitle("");
    setConclusion("");
    setPara1("");
    setPara2("");
    setPara3("");
    setPara1Links({});
    setPara2Links({});
    setPara3Links({});
    setText1("");
    setText2("");
    setText3("");
    setLink1(false);
    setLink2(false);
    setLink3(false);
    setCount(0);
    setlink1Count(0);
    setlink2Count(0);
    setlink3Count(0);
  }

  const startOver = async (e) => {
    setParaArray([]);
    setgenerate(false);
    setForm(true);
  }

  const handleTitle = async (e) => {
    setTitle(e.target.value);
  }


  const handleCount = async (e) => {
    setCount(e.target.value);
  }

  const firstFormSubmit = async (event) => {
    event.preventDefault();
    setForm(false);
    setFormTwo(true);
  }


  return (
    <>
    <div className="row">
      <div className="column text-center" id="top-row">
        <h1>Welcome To</h1>
        <h1>HTML Formatter</h1>
      </div>
    </div>
    <div className="row">
      <div className="column d-flex justify-content-center">
        {makeTitle &&
        <button onClick={toTitleForm} className="btn btn-primary" id="button-start">Let's Get Started</button>
        }
        {firstForm &&
        <form id="title-form">
          <div className="card">
            <div className="card-header first-color">
              <h3 className="text-center">
              Please Enter The Following
              </h3>
              <div id="title-labels" className="mb-3 mt-3">
                <label htmlFor="title-input" className="form-label mt-2 me-3"><b id="title-font">Title:</b></label>
                <input onChange={handleTitle} type="text" className="form-control w-50" id="title-input" value={title}></input>
              </div>
              <div id="paragraph-labels" className="mb-3 mt-3">
                <div className='col'>
                  <label id="paragraph-number-label" htmlFor="paragraph-label"><b id="number-font">Number of Paragraphs:</b></label>
                </div>
                <div className="col">
                  <div onChange={handleCount} id="paragraph-group" className="paragraph-group mt-2 mb-3" value={paragraphCount}>
                    <div className="form-check form-check-inline" id="paragraph-label">
                      <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" />
                      <label className="form-check-label" htmlFor="inlineRadio1">1</label>
                    </div>
                    <div className="form-check form-check-inline">
                      <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2" />
                      <label className="form-check-label" htmlFor="inlineRadio2">2</label>
                    </div>
                    <div className="form-check form-check-inline">
                      <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3" />
                      <label className="form-check-label" htmlFor="inlineRadio3">3</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-button d-flex">
              <button onClick={firstFormSubmit} className="btn btn-success form-one-button">Next</button>
            </div>
          </div>
        </form>
        }
        {secondForm &&
        <form id="title-form2">
          <div className="card">
            <div className="card-header first-color">
              <h3 className="text-center">Paragraphs</h3>
              <div className="d-flex justify-content-center">
                <div className="row w-75">
                  {Array(parseInt(paragraphCount)).fill(0).map((x, index) => (
                        <div key={index + 1} id="para-form-group">
                          <div className="d-flex">
                            <div className="col">
                              <label htmlFor={`paragraph${index + 1}`} className="text-start"><b>Paragraph {index + 1}</b></label>
                            </div>
                            <div className="col me-4 ms-3">
                              <input onChange={handleParaTile} key={index + 1} id={`paragraph${index + 1}Header`} className="text-center" type="text"></input>
                            </div>
                            <div className="justify-self-end">
                              <button onClick={handleLink} id={`paraButton${index + 1}`} type="button" className="btn btn-warning link-button" value={index + 1}>Add Link</button>
                            </div>
                          </div>
                            <div>
                              <textarea onChange={handleText} id={`paragraph${index + 1}`} rows="5" cols="75" className="text-field-area"></textarea>
                            </div>
                            <div>
                              {link1Added && Array(link1Count).fill(0).map((x, ind) => {
                                if (index + 1 === 1) {
                                  return (
                              <div key={`linkLabel${ind + 1}`} className="mb-2">
                                <label htmlFor={`linkSRC${ind + 1}`} className="form-label"><b>Link {ind + 1} URL</b></label>
                                <input id={`linkSRC${index + 1}-${ind + 1}`} onChange={handlelinkURL} type="url" className="form-control"></input>
                              </div>)
                                }
                              }
                              )}
                              {link2Added && Array(link2Count).fill(0).map((x, ind) => {
                                if (index + 1 === 2) {
                                  return (
                              <div key={`linkLabel${ind + 1}`} className="mb-2">
                                <label htmlFor={`linkSRC${ind + 1}`} className="form-label"><b>Link {ind + 1} URL</b></label>
                                <input id={`linkSRC${index + 1}-${ind + 1}`} onChange={handlelinkURL} type="url" className="form-control"></input>
                              </div>)
                                }
                              }
                              )}
                              {link3Added && Array(link3Count).fill(0).map((x, ind) => {
                                if (index + 1 === 3) {
                                  return (
                              <div key={`linkLabel${ind + 1}`} className="mb-2">
                                <label htmlFor={`linkSRC${ind + 1}`} className="form-label"><b>Link {ind + 1} URL</b></label>
                                <input id={`linkSRC${index + 1}-${ind + 1}`} onChange={handlelinkURL} type="url" className="form-control"></input>
                              </div>)
                                }
                              }
                              )}
                            </div>
                        </div>
                  ))}
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <label htmlFor="conclusion" className="text-center"><b>Conclusion</b></label>
                </div>
              </div>
              <div className="container justify-content-center w-75">
                <div className="row">
                  <textarea onChange={handleConclusion} id="conclusion" rows="5" cols="75"></textarea>
                </div>
                <div className="row pb-4">
                  <button onClick={generate} className="btn btn-success">Generate</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        }
        {generatedHTML &&
        <div className="gen-card">
          <div className="card">
            <div className="card-header first-color">
              <h1 className="text-center gen-head-color">Generated HTML</h1>
            </div>
            <div className="card-body gen-body first-color">
              <div className="display-area">
                <div className="text-display">
                {paragraphArray.map((x, value) => (
                  <p className="html-display">{x}</p>
                ))}
                </div>
              </div>
            </div>
            <div className="card-footer first-color">
              <div className="container">
                <div className="row text-center">
                  <h2 className="display-color">Thank you for use HTML Formatter</h2>
                  <h4 className="display-color">Would you like to start over?</h4>
                </div>
              <div className="row d-flex justify-content-center pb-2">
                <button onClick={startOver} className="btn btn-primary start-button w-25">Start Over</button>
              </div>
              </div>
            </div>
          </div>
        </div>
        }
      </div>
    </div>
    </>
  );
}

export default App;
